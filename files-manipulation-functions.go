package main

import (
	"os"
	"strings"
)

func osReadDir(root string, suffix string) ([]string, error) {
	var files []string
	f, err := os.Open(root)
	if err != nil {
		return files, err
	}
	fileInfo, err := f.Readdir(-1)
	f.Close()
	if err != nil {
		return files, err
	}

	for _, file := range fileInfo {
		/* debug info
		fmt.Print(file.Name())
		fmt.Print(" isDir: " + strconv.FormatBool(file.IsDir()))
		fmt.Println(" hasSuffix '" + suffix + "' : " + strconv.FormatBool(strings.HasSuffix(file.Name(), suffix)))
		*/
		if !file.IsDir() && strings.HasSuffix(file.Name(), suffix) {
			files = append(files, file.Name())
		}
	}
	return files, nil
}
