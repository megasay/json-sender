package main

import (
	"bufio"
	"flag"
	"fmt"
	"io/ioutil"
	"net"
	"os"
	"strconv"
)

func main() {

	// Parse comand line arguments
	var serverConnectionParams = flag.String("server", "127.0.0.1:80", "Server IP address and port. Format - host:port.")
	flag.Parse()

	// Try connet to server
	fmt.Print("Try to connect server (" + *serverConnectionParams + ")...")
	c, err := net.Dial("tcp", *serverConnectionParams)
	if err != nil {
		fmt.Println("FAILED")
		fmt.Println(err)
		return
	}
	fmt.Println("OK")

	// Print *.json files from current folder
	jsonFiles, _ := osReadDir("./", ".json")
	fmt.Println("JSON file in current folder:")
	for index, file := range jsonFiles {
		fmt.Println(index, file)
	}

	// Select file for send to server
	readerStdin := bufio.NewReader(os.Stdin)
	fmt.Println("Enter JSON file number for send it to server:")
	response, _ := readerStdin.ReadString('\n')
	last := len(response) - 1
	response = response[:last]
	selJSONFileNum, err := strconv.Atoi(response)
	if err != nil {
		fmt.Println("ERROR: Undefined number. ")
		fmt.Println(err)
		return
	}
	fmt.Println("Selected: " + jsonFiles[selJSONFileNum])

	// Readfile content
	fmt.Print("File reading...")
	content, err := ioutil.ReadFile(jsonFiles[selJSONFileNum])
	if err != nil {
		fmt.Println("FAILED")
		fmt.Println(err)
		return
	}
	fmt.Println("OK")

	// Convert []byte to string and print to screen
	strContent := string(content)
	fmt.Print("File content: " + strContent)

	// Send data to server
	fmt.Print("Sending...")
	fmt.Fprintf(c, strContent+"\n")
	fmt.Println("OK")

	// Wait response and print it
	fmt.Print("Wait for response...")
	response, _ = bufio.NewReader(c).ReadString('\n')
	if err != nil {
		fmt.Println("FAILED")
		fmt.Println(err)
		return
	}
	fmt.Println("OK")
	fmt.Println("Response: " + response)

}
